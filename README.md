[![pipeline status](https://gitlab.com/jeremymtan/jeremytan-ids721-week10/badges/main/pipeline.svg)](https://gitlab.com/jeremymtan/jeremytan-ids721-week10/-/commits/main)
# JeremyTan IDS721 Week10
The purpose of this project is to create a serverless AWS Lambda function using the Rust programming language and Cargo Lambda to deploy a local llm inference endpoint. The project focuses on AWS Lambda functionality and the limitations of deploying a llm with only 3GB of memory. I use the `rustformers` library and download a quantized model from Hugging Face. 

For new, recent AWS users, you are only allowed to have a maxmimum of 3GB of memory, making the inference endpoint take very long since we can't use the true maximum of 10GB of memory.

## Inference Endpoint 

Use default query "Faker is Jaxn because":
https://3y2m5cxxwtrjqkivdkjtoazfcq0vkjul.lambda-url.us-east-1.on.aws/

Query with example params: 
https://3y2m5cxxwtrjqkivdkjtoazfcq0vkjul.lambda-url.us-east-1.on.aws/?query='I like tennis because'

## Testing Different Models 
Openllama takes to long and can only produce 10 tokens before risking a timeout on the lambda function (max is 15 minutes)

![Screenshot_2024-04-13_at_5.45.45_PM](/uploads/00eb0744848f8d60a4834d43ac2e4790/Screenshot_2024-04-13_at_5.45.45_PM.png)


I then find a smaller model called Pythia that has less tensors and the size is smaller 2gb to 500mb. This model can run 20 tokens before risking a timeout on the lambda function. 

![Screenshot_2024-04-13_at_5.45.50_PM](/uploads/2d58aa4bbc76bb4877c8f2d52cc6e7b7/Screenshot_2024-04-13_at_5.45.50_PM.png)

## Preparation 
1. Make a cargo lambda HTTP function `cargo lambda new <folder name>`
2. Choose a hugging face model available to use that works with `rustformers` (really only two choices as the model needs to be small enough to actually run on lambda): https://huggingface.co/rustformers
3. Modify the template with your own additions or deletions.
4. Add an inference endpoint when you use the `rustfomers` package (make sure to add my Cargo.toml dependencies)
5. Do `cargo lambda watch` to test locally.
6. Confirm your inference endpoint actually works. A rule of thumb is for every 2 second it takes to run the model locally it will take an addtional 1.5 minutes to run with your lambda function
7. Go to the AWS IAM web management console and add an IAM User for credentials.
8. Attach policies `lambdafullaccess` and `iamfullaccess`.
9. `source` your `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, and `AWS_REGION`
10. Go to ECR in AWS and create a new private repository. If you have an existing one, feel free to delete it so you can incur less charges (no matter what this project will incur a charge due to the model itself being over 500mb way over the free tier limit)
11. Follow the instuctions 
12. Replace step 2 with `docker buildx build --progress=plain --platform linux/arm64 -t <your image name here> .`
13. Once you have the image pushed, go to lambda, create new function, deploy with image, choose `arm64`
14. Wait for it to be fully deployed
15. Go to `Configuration` -> `General Configuration` -> edit -> change memory to the max limit of 3008 (as new users can't go past that) -> change timeout to the max of 15 minutes (as it takes the model a long time to compute)
16. Go back to `Configuration` -> `Fucntion URL` -> create a new function URL -> choose NONE not IAM -> keep BUFFERED -> enable CORS
17. Test your fucntion URL, it should take around 8 minutes to run if you use my model 
18. Add your model (the .bin file) to git lfs otherwise you can't push and will have to deal with errors

## References
1. https://huggingface.co/rustformers
2. https://huggingface.co/rustformers/pythia-ggml/tree/main
3. https://docs.rs/llm/latest/llm/struct.InferenceRequest.html
4. https://docs.gitlab.com/ee/topics/git/lfs/
5. https://doc.rust-lang.org/rust-by-example/flow_control/match.html
6. https://www.cargo-lambda.info/commands/watch.html
7. https://aws.amazon.com/ecr/pricing/
8. https://stackoverflow.com/questions/31080757/copy-files-to-the-target-directory-after-build
9. https://medium.com/@seanbailey518/deploy-serverless-generative-ai-on-aws-lambda-with-openllama-793f97a7cbdd
10. https://github.com/rustformers/llm?tab=readme-ov-file
